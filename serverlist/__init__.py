""" Main entry point. """
from pyramid.config import Configurator


def main(global_config, **settings):
    config = Configurator(settings=settings)
    config.include("cornice")
    config.add_route('index', '/')
    config.scan("serverlist.views")
    config.add_static_view(name='static', path='serverlist:static')
    return config.make_wsgi_app()
