// main app-file

// Load the application once the DOM is ready, via jQuery.ready
$(function() {

    // Server Model
    // ============
    // it's a single list entry
    var Server = Backbone.Model.extend({

        defaults: function() {
            return {
                addr: '127.0.0.1',
                port: 80,
                ssl: true
            }
        },

        toggleSsl: function() {
            this.save({ssl: !this.get('ssl')});
        }
    });

    // exported from backbone for fetch overriding
    var wrapError = function(model, options) {
        var error = options.error;
        options.error = function(resp) {
            if (error) error(model, resp, options);
            model.trigger('error', model, resp, options);
        };
    };


    // Server Collection
    // =================
    // the whole list
    var ServerList = Backbone.Collection.extend({

        // reference to model
        model: Server,

        url: '/servers',

        // default fetch is flawed because it's awaiting list in ajax
        // (see e.g. http://flask.pocoo.org/docs/security/#json-security)
        fetch: function(options) {
            options = options ? _.clone(options) : {};
            if (options.parse === void 0) options.parse = true;
            var success = options.success;
            var collection = this;
            options.success = function(resp) {
                var method = options.reset ? 'reset' : 'set';
                collection[method](resp.servers, options);
                if (success) success(collection, resp, options);
                collection.trigger('sync', collection, resp, options);
            };
            wrapError(this, options);
            return this.sync('read', this, options);
        },

        // sort by insertion order
        comparator: 'id'
    });

    var servers = new ServerList; // global collection to work with

    // Server View
    // ===========
    var ServerView = Backbone.View.extend({

        tagName: 'li',

        template: _.template($('#server-template').html()),

        events: {
            'click .checkbox': 'toggleSsl',
            'dblclick .addr'  : 'editAddr',
            'dblclick .port'  : 'editPort',
            'click a.destroy': 'clear',
            'keypress .edit' : 'updateOnEnter',
            'blur .edit'     : 'stopEditing'
        },

        initialize: function() {
            // when a model's attributes have changed
            this.listenTo(this.model, 'change', this.render);
            // when a model is destroyed
            this.listenTo(this.model, 'destroy', this.remove);
        },

        render: function() {
            this.$el.html(this.template(this.model.toJSON()));
            this.inputAddr = this.$('.edit-addr');
            this.inputPort = this.$('.edit-port');
            return this;
        },

        toggleSsl: function() {
            this.model.toggleSsl();
        },

        // start editing address field
        editAddr: function() {
            this.$el.addClass('editing')// make to concrete form
            this.inputAddr.focus()
        },

        editPort: function() {
            this.$el.addClass('editing')// make to concrete form
            this.inputPort.focus()
        },


        // stop editing, saving changes
        stopEditing: function() {
            var valAddr = this.inputAddr.val(),
                valPort = this.inputPort.val();
            if (!valAddr || !valPort) {
                this.clear();
            } else {
                this.model.save({addr: valAddr, port: valPort});
                this.$el.removeClass('editing');
            }
        },

        updateOnEnter: function(e) {
            if (e.keyCode == 13) this.stopEditing();
        },

        // destroy the model and thus remove the item (see listenTo at this.initialize)
        clear: function() {
            this.model.destroy();
        }
    });

    // The server-list app
    // ===================
    var SlAppView = Backbone.View.extend({

        el: $('#serverlist'),

        footerTemplate: _.template($('#footer-template').html()),

        events: {
            'keypress #new-addr': 'createOnEnter',
            'keypress #new-port': 'createOnEnter',
            'click #btn-add'    : 'doInsert',
            'click #clear-list' : 'clearList'
        },

        initialize: function() {
            this.addrInput = this.$('#new-addr');
            this.portInput = this.$('#new-port');
            this.sslCheckbox = this.$('#new-ssl');

            // when a model is added to a collection
            this.listenTo(servers, 'add', this.addOne);
            // when the collection's entire contents have been replaced
            this.listenTo(servers, 'reset', this.addAll);
            // this special event fires for any triggered event, passing the event name as the first argument
            this.listenTo(servers, 'all', this.render);

            this.footer = this.$('#footer');
            this.listSect = this.$('#list-sect');

            servers.fetch();
        },

        render: function() {
            var count = servers.length;
            if (count) {
                this.listSect.show();
                this.footer.show();
                this.footer.html(this.footerTemplate({count: count}));
            } else {
                this.listSect.hide();
                this.footer.hide();
            }
        },

        addOne: function(servModel) {
            var view = new ServerView({model: servModel});
            // add to ul the brand new li element from ServView.render
            this.$('#list').append(view.render().el);
        },

        addAll: function() {
            servers.each(this.addOne, this);
        },

        doInsert: function() {
            if (!this.addrInput.val() || !this.portInput.val()) return;
            servers.create({
                addr: this.addrInput.val(),
                port: this.portInput.val(), 
                ssl: this.sslCheckbox.prop('checked')
            });
            // reset all new server elements
            this.addrInput.val('');
            this.portInput.val('');
            this.sslCheckbox.prop('checked') = false;
        },

        createOnEnter: function(e) {
            if (e.keyCode != 13) return;
            this.doInsert();
        },

        clearList: function() {
            // send delete request
        }
    });

    var app = new SlAppView();
});
