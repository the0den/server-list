""" Cornice services. """
import json
import os

from pyramid.response import FileResponse
from pyramid.view import view_config
from cornice import Service


@view_config(route_name='index')
def index_view(request):
    here = os.path.dirname(__file__)
    index = os.path.join(here, 'templates', 'index.html')
    return FileResponse(index, request=request)


SERVER_FIELDS = ('addr', 'port', 'ssl')


_servers_storage = {}  # our server-list in memory
servers = Service(
    name='servers',
    path='/servers',
    description='Server list'
)


@servers.get()
def get_server_list(request):
    return {'servers': list(_servers_storage.values())}


def check_serv_data(request, need_all_fields):
    try:
        server = json.loads(
            request.body.decode('utf-8')
        )
    except:
        request.errors.add('body', 'syntax', 'Bad JSON format')
        return
    if need_all_fields:
        all_fields_exist = True
        for field in SERVER_FIELDS:
            if not field in server:
                request.errors.add('body', field, 'Absent field')
                all_fields_exist = False
        if not all_fields_exist:
            return
    if 'port' in server:
        try:
            server['port'] = int(server.get('port'))
        except:
            request.errors.add('body', 'port', 'Bad integer')
    if 'ssl' in server:
        server['ssl'] = bool(server.get('ssl'))
    if not request.errors:
        request.validated['server'] = server


def check_serv_data_on_insert(request):
    check_serv_data(request, True)


def check_serv_data_on_update(request):
    """
    Validator for a server data when updating.

    Actually, we don't need all the fields to present on update.
    """
    check_serv_data(request, False)


def get_next_id():
    i = 0
    while True:
        yield i
        i += 1

id_generator = get_next_id()

@servers.post(validators=check_serv_data_on_insert)
def add_server(request):
    new_server = {}
    for field in SERVER_FIELDS:
        new_server[field] = request.validated['server'][field]
    new_id = id_generator.__next__()
    new_server['id'] = new_id
    _servers_storage[new_id] = new_server
    return {'status': 'added', 'id': new_id}


@servers.delete()
def clear_server_list(request):
    _servers_storage.clear()
    return {'status': 'cleared'}


server = Service(
    name='server',
    path='/servers/{id}',
    description='Single server in the list'
)


def check_list_id(request):
    try:
        id = int(request.matchdict['id'])
    except:
        request.errors.add(
            'querystring', 'id', 'Entry id must be an integer'
        )
        return
    if id in _servers_storage:
        request.validated['id'] = id
    else:
        request.errors.add(
            'querystring', 'id', 'Entry id not found'
        )


@server.put(validators=(check_list_id, check_serv_data_on_update))
def update_server(request):
    for field in request.validated['server']:
        if field in SERVER_FIELDS:
            _servers_storage[request.validated['id']][field] = (
                request.validated['server'][field]
            )
    return {'status': 'updated', 'position': request.validated['id']}


@server.delete(validators=check_list_id)
def delete_server(request):
    del _servers_storage[request.validated['id']]
    return {'status': 'deleted', 'position': request.validated['id']}

